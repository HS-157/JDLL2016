# JDLL2016
Diaporama de la Maison du Libre pour les JDLL 2016 à Lyon

Ce diaporama a été généré par [Pandoc](http://pandoc.org/) et utilise [Slideous](http://goessner.net/articles/slideous/) avec le css modifié.

Commande pour générer la page HTML :  
```pandoc -s -t slideous -i --slide-level=1 -f markdown slide.md -o index.html```

Pour que cette page marche, il faut le css, le javascript et les médias.

Le css de base et le javascript est récupérable [ici](http://goessner.net/download/prj/slideous/) et le diaporama marche avec.

Les médias correspond au dossier image/ de ce git.

Pour utiliser le css modifié, il se situe dans slideous/slideous.css de ce git.

Pandoc peut générer une page html en standalone avec tout dedans, css, javascript et médias grace à l'option `--self-contained` et la commande :  
```pandoc -s -t slideous --self-contained -i --slide-level=1 -f markdown slide.md -o standalone.html```

Le script commande.sh intègre ces deux commandes et permet de générer ses deux pages html plus facilement.

Le diaporama est sous licence `CC-BY-SA` [CC-Attribution-ShareAlike](https://github.com/idleberg/Creative-Commons-Markdown/blob/spaces/4.0/by-sa.markdown)

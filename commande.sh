#/bin/bash
pandoc -s -t slideous -i --slide-level=1 -f markdown slide.md -o index.html
pandoc -s -t slideous --self-contained -i --slide-level=1 -f markdown slide.md -o standalone.html

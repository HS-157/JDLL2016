% Maison du Libre
% Hervé Sousset
% JDLL 2016 - Lyon - 3 avril 2016

# Maison du Libre
## Maison du Libre
Hervé Sousset

JDLL 2016 - Lyon - 3 avril 2016

[jdll2016.hs-157.moe](http://jdll2016.hs-157.moe)

[github.com/HS-157/JDLL2016](https://github.com/HS-157/JDLL2016)


# Hervé Sousset
## Hervé Sousset

- Internets / Minitels :
	- HS-157
	- HS\_157  
	- [\@HS\_157](https://twitter.com/HS_157)
<p></p>
- Contact :
	- <hs-157@mdl29.net> ([0x4ABD7A2F](https://sks-keyservers.net/pks/lookup?op=vindex&search=hs-157))
	- IRC : Freenode
	- XMPP : [hs-157@mdl29.net](xmpp:hs-157@mdl29.net?message)

- GEII - Brest

- Archlinux

- Électronique

- Informatique

- Culture japonaise

- Culture Libre

- Adhérent MDL

# Le Libre à Brest
## Le Libre à Brest

- 1995 - [Infini](http://www.infini.fr/)

- 1997 - [Finix](http://finix.eu.org/blog/)

- 2002 - Archipel du Libre

- 2003 - [Brest-Wireless](http://brest-wireless.net/wiki/)

- 2010 - [Maison du Libre](MDL29)

- 2013 - [Les Chats Cosmiques](http://leschatscosmiques.net/)


# Maison du Libre - Présentation
## Maison du Libre - Présentation
- 128 adhérents

- 60 jeunes

- Local : 640 m^2^

- Budget 2016 : 42 000 / 50 000 €

- Animations :
	- Jeudi soir
	- Vendredi soir
	- Samedi journée
<p></p>
- [\@MDL29\_](MDL29_)
- [mdl29.net](MDL29)

# Maison du Libre
## Maison du Libre
<div class="image">
[![](image/localmdl1.jpg)](image/localmdl1.jpg)
</div>

- 2010 : Création
	- Rassemblement :
		- Archipel du Libre
		- Brest-wireless
		- Finix
		- Infini
<p></p>
- Local :
	- 640 m^2^
	- Tiers-lieux :
		- Associations :
			- [Maison du Libre](MDL29)
			- [POC La Girafe](http://www.poclagirafe.bzh/)
		- Entrepreneurs :
			- Développeur
			- Analyste / Stratégie
			- Dessinatrice / Artiste
			- Attachée presse
			- Géomaticienne
		- Particuliers

# Activités - Libranet
## Libranet
![](image/logo/libranet.png)

- 2010

- Objectif :
	- Courriel
	- Blog
	- Messagerie instantanée

- Outils Libre

- Plateforme libre

- [Documentation](https://wiki.mdl29.net/doku.php?id=plateforme_mdl:intro)

- [mdl29.net/projets/libranet/](http://mdl29.net/projets/libranet/)

# Activités - Tyfab
## Tyfab
<div class="image">
[![](image/tyfab.jpg)](image/tyfab.jpg)
</div>
![](image/logo/tyfab.png)

- 2012

- 1^er^ fablab associatif brestois

- Outils :
	- Imprimante 3D
	- Découpeuse Laser
	- Fraiseuse numérique
	- Découpeuse vinyl
	- Tour
	- Outils standard

- [Wiki](http://wiki.tyfab.fr/doku.php)

- Réalisations sous licence Libre

- Réalisations :
	- [Table à LED](http://wiki.tyfab.fr/doku.php?id=projets:table_a_led:accueil)
	- [Smoothie Board](http://smoothieware.org/smoothieboard)
	- [Bareur de Bateau](http://www.splashelec.com/)
	- Réparation / Amélioration de machine
	- Open Path View
	- Projets personnels

- [\@Tyfab](tyfab)

- [tyfab.fr](tyfab)

# Activités - Les Petits Hackers
## Les Petits Hackers
<div class="images">
<figure>
[![](image/lph/lph1.jpg)](image/lph/lph1.jpg)
[![](image/lph/lph2.jpg)](image/lph/lph2.jpg)
[![](image/lph/lph3.jpg)](image/lph/lph3.jpg)
[![](image/lph/lph4.jpg)](image/lph/lph4.jpg)
[![](image/lph/lph1.jpg)](image/lph/lph1.jpg)
</figure>
</div>
![](image/logo/lph.png)

- 2010 / 2012

- Jeunes :
	- Matin : 8 - 12 ans
	- Après-midi : 12 - 18 ans

- Animatrices

- Électronique

- Informatique

- Réalisations :
	- [Tidu / Tyzef](https://github.com/mdl29/tidutyzef)
	- [Pano Bus](https://github.com/mdl29/panobus)
	- Robots

- [les-petits-hacker.mdl29.net](http://les-petits-hackers.mdl29.net/)

# Activités - Open Path View
## Open Path View
<div class="images">
<figure>
[![](image/opv/opv-sac.jpg)](image/opv/opv-sac.jpg)
[![](image/opv/opv-sac2.jpg)](image/opv/opv-sac2.jpg)
[![](image/opv/opv-sac3.jpg)](image/opv/opv-sac3.jpg)
[![](image/opv/pano1.png)](image/opv/pano1.png)
[![](image/opv/opv-sac.jpg)](image/opv/opv-sac.jpg)
</figure>
</div>
![](image/logo/opv.png)

- 2013

- Photos sphériques

- Licence Libre

- Là où G ne va pas

- Lieux :
	- Brest
	- Pointe St-Mathieu
	- Vielles Charrues
	- ...
<p></p>
- Partenaires :
	- Association Bertheaume
	- Association Les Amis de St-Mathieu
	- Ville de Brest
	- Conservatoire Botanique National de Brest
	- Association Guipavas Identité Patrimoine 
	- Commune de Lanhouarneau
	- Région Bretagne

- [\@OpenPathView](opv)

- [OpenPathView.fr](OpenPathView.fr) / [opv.li](http://opv/li)

# Activités - Autre
## Activités - Autre

- Jeudi soir :
	- OpenLab
<p></p>
- Vendredi soir :
	- MDL Café
	- Atelier du vendredi soir
<p></p>
- Samedi :
	- Matin : Les Petits Hackers - 8 / 12 ans
	- Après-midi : Les Petits Hackers - 12 / 18 ans

# Évenements
## Évenements

- Récurrent :
	- Open Bidouille Camp / Junior
	- Maker Faire
	- Install Party

- RMLL : Nantes / Beauvais

- Journée Finistérienne du Libre

- Coding Week

- Vieilles Charrues

- JDLL 2016

- Amis :
	- Bégard : Infothema
	- Quimper : Linux Quimper
	- Morlaix - Finix Morlaix

# Contact
## Contact

- Courriel : <maison-du-libre@mdl29.net>

- XMPP : [maison-du-libre@conference.mdl29.net](xmpp:maison-du-libre@conference.mdl29.net?join)

- IRC : [chat.freenode.net/#mdl29](irc://chat.freenode.net/#mdl29)

- Gazouille :
	- [Maison du Libre](MDL_29)
	- [Tyfab](tyfab)
	- [Open Path View](opv)

- Local : 214 rue Jean Jaurès, 29200 Brest

- [mdl29.net/association/contact/](http://mdl29.net/association/contact/)

- ## Questions ?

- [mdl29.net](MDL29)

- CC-BY-SA + Beerware - Maison du Libre Brest

[MDL29]:http://mdl29.net/
[MDL29_]:(https://twitter.com/mdl29_)
[tyfab]:(https://twitter.com/TyFabBrest)
[opv]:(https://twitter.com/OpenPathView)
